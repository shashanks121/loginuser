var express = require('express');
var router = express.Router();


/* GET test page. */
router.get('/test', function(req, res, next) {
  res.render('test');
});
router.get('/layout',function(req,res,next){
  res.render('layout');
});
router.post('/showusers',function(req,res,next){
  res.render('showRegisteredUser');
});
router.get('/forgotPassword',function(req,res,next){
  res.render('forgotPassword');
});
module.exports = router;